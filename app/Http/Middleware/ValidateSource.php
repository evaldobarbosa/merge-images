<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ValidateSource
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        // dd(config('services.mergeimg.token'));
        if (empty(config('services.mergeimg.token'))) {
            abort(401);
        }

        if (config('services.mergeimg.token') !== $request->header('X-CLIENT-SECRET')) {
            abort(401);
        }

        return $next($request);
    }
}
