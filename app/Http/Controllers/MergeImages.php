<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MergeImages extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $frenteSource = base64_decode($request->get('frente'));
        $versoSource = base64_decode($request->get('verso'));
        // dd($frenteSource);

        $frente = imagecreatefromstring($frenteSource);
        $verso = imagecreatefromstring($versoSource);

        $frenteW = imagesx($frente);
        $frenteH = imagesy($frente);
        $versoW = imagesx($verso);
        $versoH = imagesy($verso);

        $largura = ($frenteW > $versoW) ? $frenteW : $versoW;

        ob_start();

        $nova = imagecreatetruecolor($largura, ( $frenteH + $versoH ) * 1.1);

        imagecopymerge($nova, $frente, 10, 10, 0, 0, $frenteW, $frenteH, 100);
        imagecopymerge($nova, $verso, 10, $frenteH + 10, 0, 0, $versoW, $versoH, 100);

        imagejpeg($nova, NULL, 100);
        $data = ob_get_contents();
        ob_end_clean();

        // imagepng($nova);
        imagedestroy($nova);
        imagedestroy($frente);
        imagedestroy($verso);

        return response()->json([
            "content" => base64_encode($data),
            // "data" => "<img src='data:image/jpeg;base64," . base64_encode( $data )."'>"
        ]);
    }
}
